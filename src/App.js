import { useState } from "react";

import styled from "styled-components";

import { Link, Route, Switch } from "react-router-dom";
import { FiStar, FiHome } from "react-icons/fi";
import Characters from "./pages/Characters";
import FavoriteCharacters from "./pages/FavoriteCharacters";

export default function App() {
  const [favorites, setFavorites] = useState(() => {
    const favoriteList = localStorage.getItem("favoriteList");
    if (favoriteList) {
      return JSON.parse(favoriteList);
    }
    return [];
  });

  return (
    <div className="App">
      <div className="App-header">
        <HeaderLinks>
          <Link to="/">
            <FiHome />
            Home
          </Link>
          <Link to="/favorite">
            <FiStar />
            Favorites
          </Link>
        </HeaderLinks>
        <Switch>
          <Route exact path="/">
            <Characters favorites={favorites} setFavorites={setFavorites} />
          </Route>
          <Route path="/favorite">
            <FavoriteCharacters favorites={favorites} setFavorites={setFavorites} />
          </Route>
        </Switch>
      </div>
    </div>
  );
}

const HeaderLinks = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;

  a {
    margin: 0.5rem 5rem;
    font-weight: bold;
    color: whitesmoke;

    display: flex;
    align-items: center;

    svg {
      margin-right: 0.4rem;
    }
  }

  @media only screen and (max-width: 560px) {
    a {
      margin: 0.5rem 2vw;
    }
  }
`;
