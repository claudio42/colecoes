import { useState, useEffect } from "react";
import { FiArrowRight, FiArrowLeft } from "react-icons/fi";

import Card from "../Components/Card";
import ChangeTopicButton from "../Components/ChangeTopicButton";
import SearchInput from "../Components/SearchInput";

import { filterCharacters } from "../utils/filterCharacters";

import axios from "axios";
import styled from "styled-components";

const Characters = ({ setFavorites, favorites }) => {
  const [page, setPage] = useState(1);
  const [isRickAndMorty, setIsRickAndMorty] = useState(true);
  const [characters, setCharacters] = useState([]);

  const [search, setSearch] = useState("");

  const TOTAL_POKEMONS = 150;
  const POKEMONS_PER_PAGE = 10;

  const getRickAndMorty = async () => {
    const res = await axios.get(`https://rickandmortyapi.com/api/character?page=${page}`);
    setCharacters(res.data.results);
  };

  const getPokemons = async () => {
    const res = await axios.get(
      `https://pokeapi.co/api/v2/pokemon?offset=${
        (page - 1) * POKEMONS_PER_PAGE
      }&limit=${POKEMONS_PER_PAGE}`
    );
    setCharacters(res.data.results);
  };

  useEffect(() => {
    isRickAndMorty ? getRickAndMorty() : getPokemons();
    // eslint-disable-next-line
  }, [page, isRickAndMorty]);

  const handleNextPage = () => {
    // Pagination limits on Rick and Pokemon
    if (isRickAndMorty && page >= 35) return;
    if (!isRickAndMorty && page >= TOTAL_POKEMONS / POKEMONS_PER_PAGE) return;
    setPage(state => state + 1);
  };

  const handlePreviousPage = () => {
    if (page <= 1) return;
    setPage(state => state - 1);
  };

  const handleChangeTopic = () => {
    setPage(1);
    setIsRickAndMorty(!isRickAndMorty);
  };

  const filteredCharacters = filterCharacters(characters, search);

  return (
    <>
      <small>Search in your page:</small>
      <SearchInput value={search} setValue={setSearch} />
      <ChangeTopicButton action={handleChangeTopic} isRickAndMorty={isRickAndMorty} />
      <h5>You're on page {page} </h5>
      <ButtonContainer>
        <Button onClick={handlePreviousPage}>
          <FiArrowLeft />
          Previous page
        </Button>
        <Button onClick={handleNextPage}>
          Next page
          <FiArrowRight />
        </Button>
      </ButtonContainer>
      <CardsDiv>
        {filteredCharacters.map((char, idx) => {
          let isRemovable;

          favorites.forEach(element => {
            if (element.name === char.name) {
              isRemovable = true;
            }
          });

          return (
            <Card
              key={idx}
              isRickAndMorty={isRickAndMorty}
              character={char}
              setFavorites={setFavorites}
              favorites={favorites}
              isRemovable={isRemovable}
            />
          );
        })}
      </CardsDiv>
    </>
  );
};

const Button = styled.button`
  background-color: #77aca2;
  color: white;

  padding: 1rem 1.5rem;
  margin: 1rem 2rem;

  outline: none;
  border: none;
  border-radius: 20px;

  display: flex;
  align-items: center;

  font-size: 1.1rem;
  font-weight: bold;

  transition: 0.15s ease-in;

  cursor: pointer;

  :hover {
    transform: scale(1.06);
    background-color: #4e7e76;
  }

  svg {
    margin: 0 0.35rem;
  }
`;

const CardsDiv = styled.div`
  padding: 2rem 12rem;
  background-color: #468189;
  border-radius: 15px;

  display: grid;
  grid-template-columns: 1fr 1fr;
  gap: 45px;

  @media only screen and (max-width: 840px) {
    padding: 2rem 0;
    width: 95%;
  }

  @media only screen and (max-width: 450px) {
    grid-template-columns: 1fr;
  }
`;

const ButtonContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
`;

export default Characters;
