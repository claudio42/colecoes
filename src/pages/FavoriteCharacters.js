import { useState } from "react";

import styled from "styled-components";

import { filterCharacters } from "../utils/filterCharacters";

import Card from "../Components/Card";
import ChangeTopicButton from "../Components/ChangeTopicButton";
import SearchInput from "../Components/SearchInput";

const FavoriteCharacters = ({ favorites, setFavorites }) => {
  const [isRickAndMorty, setIsRickAndMorty] = useState(true);
  const [search, setSearch] = useState("");

  const handleChangeTopic = () => {
    setIsRickAndMorty(!isRickAndMorty);
  };

  // character.image returns undefined if it's a pokemon
  const favoritesRickAndMorty = favorites.filter(character => character.image);
  const favoritesPokemon = favorites.filter(character => !character.image);

  const filteredRickAndMorty = filterCharacters(favoritesRickAndMorty, search);
  const filteredPokemons = filterCharacters(favoritesPokemon, search);

  const renderCards = charactersArray => {
    return charactersArray.map((char, idx) => {
      return (
        <Card
          key={idx}
          isRickAndMorty={isRickAndMorty}
          character={char}
          setFavorites={setFavorites}
          favorites={favorites}
          isRemovable
        />
      );
    });
  };

  const renderEmptyMessage = topic => {
    return <p>{`There isn't any ${topic} favorites.`}</p>;
  };

  return (
    <>
      <FavoritesTitle>Favorite List</FavoritesTitle>
      <SearchInput value={search} setValue={setSearch} />
      <ChangeTopicButton
        action={handleChangeTopic}
        isRickAndMorty={isRickAndMorty}
        isDisabled={favorites.length === 0}
      />
      {favorites.length > 0 ? (
        <CardsDiv>
          {isRickAndMorty
            ? favoritesRickAndMorty.length > 0
              ? renderCards(filteredRickAndMorty)
              : renderEmptyMessage("Rick & Morty")
            : favoritesPokemon.length > 0
            ? renderCards(filteredPokemons)
            : renderEmptyMessage("Pokemon")}
        </CardsDiv>
      ) : (
        <p>No characters were added.</p>
      )}
    </>
  );
};

const FavoritesTitle = styled.h2`
  margin: 2rem 0;
  font-weight: bold;
`;

const CardsDiv = styled.div`
  padding: 2rem 12rem;
  background-color: #468189;
  border-radius: 15px;

  display: grid;
  grid-template-columns: 1fr;
  gap: 60px;

  @media only screen and (max-width: 840px) {
    padding: 2rem 0;
    width: 95%;
  }
`;

export default FavoriteCharacters;
