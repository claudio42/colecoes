import { useEffect } from "react";
import styled from "styled-components";
import { FiSlash, FiStar } from "react-icons/fi";

const Card = ({
  isRickAndMorty,
  character,
  setFavorites,
  favorites,
  isRemovable = false,
}) => {
  const handleAddFavorite = char => {
    const alreadyExist = favorites.find(element => element.name === char.name);
    if (alreadyExist) return;
    setFavorites([...favorites, char]);
  };

  const handleRemoveFavorite = char => {
    const newList = favorites.filter(el => el.name !== char.name);
    localStorage.setItem("favoriteList", JSON.stringify(newList));
    setFavorites(newList);
  };

  const pokeId = !isRickAndMorty && character.url.split("/").reverse()[1];
  const imgSrcPokemon =
    !isRickAndMorty &&
    `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokeId}.png`;

  useEffect(() => {
    localStorage.setItem("favoriteList", JSON.stringify(favorites));
  }, [favorites]);

  return (
    <CardDiv>
      <CardName>{character.name}</CardName>
      <CardImage
        src={isRickAndMorty ? character.image : imgSrcPokemon}
        alt={character.name}
      />
      {isRemovable ? (
        <RemoveFavoriteButton onClick={() => handleRemoveFavorite(character)}>
          <TextInBtn>Remove</TextInBtn>
          <FiSlash />
        </RemoveFavoriteButton>
      ) : (
        <AddFavoriteButton onClick={() => handleAddFavorite(character)}>
          <TextInBtn>Add</TextInBtn>
          <FiStar />
        </AddFavoriteButton>
      )}
    </CardDiv>
  );
};

const CardImage = styled.img`
  width: 275px;
  object-fit: cover;
  border: 2px solid black;
`;

const CardDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  align-self: end;
`;

const CardName = styled.span`
  font-weight: bold;
  font-size: 1.3rem;

  text-align: center;

  max-width: 11rem;

  margin-bottom: 0.4rem;
`;

const TextInBtn = styled.span`
  margin-right: 0.4rem;
`;

const AddFavoriteButton = styled.button`
  padding: 0.8rem 1.5rem;
  margin: 1rem 0;

  background-color: #f4e9cd;
  font-size: 1.2rem;

  display: flex;
  align-items: center;

  border-radius: 15px;
  border: none;
  outline: none;

  cursor: pointer;

  transition: 0.2s ease-in;

  :hover {
    background-color: whitesmoke;
  }
`;

const RemoveFavoriteButton = styled.button`
  padding: 0.8rem 1.5rem;
  margin: 1rem 0;

  background-color: #e40000;
  color: whitesmoke;
  font-size: 1.2rem;

  border-radius: 15px;
  border: none;
  outline: none;

  display: flex;
  align-items: center;

  cursor: pointer;

  transition: 0.2s ease-in;

  :hover {
    background-color: #7a0000;
  }
`;

export default Card;
