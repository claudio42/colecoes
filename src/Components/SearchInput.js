import { FiSearch } from "react-icons/fi";
import styled from "styled-components";

export default function SearchInput({ value, setValue }) {
  return (
    <SearchContainer>
      <Input onChange={e => setValue(e.target.value)} value={value} maxLength={10} />
      <FiSearch size={18} color="#404040" />
    </SearchContainer>
  );
}

const SearchContainer = styled.div`
  position: relative;

  display: flex;
  align-items: center;

  margin-top: 1rem;

  svg {
    position: absolute;
    right: 5px;
  }
`;

const Input = styled.input`
  text-align: start;
  font-size: 0.8rem;
  background-color: #f8f8fc;

  height: 2rem;
  border-radius: 0.55rem;
  background: #f8f8fc;
  border: 1px solid #e6e6f0;
  outline: 0;
  padding: 0 0.55rem;
`;
