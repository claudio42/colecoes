import styled from "styled-components";
import { FiRefreshCw } from "react-icons/fi";

function ChangeTopicButton({ action, isRickAndMorty, isDisabled }) {
  return (
    <Button onClick={action} disabled={isDisabled}>
      <FiRefreshCw /> {isRickAndMorty ? "Pokemon" : "Rick and Morty"}
    </Button>
  );
}

const Button = styled.button`
  background-color: #3e655f;
  color: white;

  padding: 1rem 1.5rem;
  margin: 1rem 2rem;

  outline: none;
  border: 2px solid whitesmoke;
  border-radius: 20px;

  display: flex;
  align-items: center;

  font-size: 1.1rem;
  font-weight: bold;

  transition: 0.25s ease-in;

  cursor: pointer;

  svg {
    margin: 0 0.35rem;

    transition: transform 0.4s cubic-bezier(0.67, -0.01, 0.39, 0.97);
  }

  :hover {
    transform: scale(1.06);
    background-color: #365953;

    svg {
      transform: rotate(180deg);
      margin: 0 0.35rem;
    }
  }

  :disabled,
  [disabled] {
    display: none;
  }
`;

export default ChangeTopicButton;
