export const filterCharacters = (chars, search) =>
  chars.filter(char => {
    const name = char.name;
    const regex = new RegExp(search, "gi");

    return name.match(regex);
  });
